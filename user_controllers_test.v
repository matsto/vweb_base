module main

import vweb
import net.http
import json
import rand
import os
import time
import models
import dtos

const port = 8183
const ip = '127.0.0.1'
const base_url = 'http://${ip}:${port}'
const logger_path = os.join_path_single(os.temp_dir(), '${rand.hex(5)}.db')
const db_path = ':memory:'

fn testsuite_begin() {
	db := setup_database(db_path) or { panic(err) }
	logger := setup_logger(logger_path) or { panic(err) }
	test_app := new_app(db, logger)
	spawn vweb.run(test_app, port)
	time.sleep(1 * time.second)
}

fn testsuite_end() {
	// add cleanup here
}

// fn test_controller_get_all_users() {

// 	url:=base_url+"/users/"

// 	header := http.new_custom_header_from_map(
// 		{"token": "token"}
// 	)!
// 	f_config := http.FetchConfig{url: url, header: header}

// 	resp := http.fetch(f_config) or {
// 		assert err.msg() == ''
// 		return
// 	}
// 	assert resp.status_code == 200

// 	resp_body := json.decode([]models.User, resp.body)!
// 	assert resp_body.len == 1
// 	resp_user := resp_body[0]
// 	// assert resp_user.id == user.id, resp_user.str()
// 	// assert resp_user.password != '', resp_user.str()
// }

fn test_controller_get_all_users_without_auth() {
	url := base_url + '/users/'
	resp := http.get(url) or {
		assert err.msg() == ''
		return
	}
	assert resp.status_code == 401
	assert resp.body == 'Missing Authorization header'
}

fn test_controller_create_user() {
	url := base_url + '/users/'
	user := models.User{
		name: 'test_user'
		email: 'test_email'
		password: 'password'
	}
	req_body := json.encode(user)

	resp := http.post_json(url, req_body) or {
		assert err.msg() == '', err.msg()
		return
	}

	resp_data := json.decode(dtos.UserOutSchema, resp.body) or {
		assert err.msg() == '', err.msg()
		return
	}

	assert resp.status_code == 201

	assert resp_data.name == user.name, resp.body
	assert resp_data.id == 1, resp.body
}
