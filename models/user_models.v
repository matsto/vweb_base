module models

@[table: 'users']
pub struct User {
pub mut:
	id         int    @[primary; sql: serial]
	name       string @[required; sql_type: 'TEXT'; unique]
	email      string @[required; sql_type: 'TEXT'; unique]
	password   string @[required; sql_type: 'TEXT']
	created_at string @[default: 'CURRENT_TIMESTAMP']
	updated_at string @[default: 'CURRENT_TIMESTAMP']
	deleted_at string @[default: 'CURRENT_TIMESTAMP']
	active     bool   @[default: true]
	admin      bool   @[default: false]
}
