module main

import vweb
import db.sqlite
import log
import dtos
import os

const http_port = 8088

struct App {
	vweb.Context
pub mut:
	db     sqlite.DB
	logger log.Log          @[vweb_global]
	user   dtos.UserJWTData
}

pub fn (mut app App) before_request() bool {
	log_request_middleware(mut app)
	return auth_middleware(mut app)
}

fn main() {
	mut db := setup_database('database.db')!
	defer {
		db.close() or {
			eprintln('DB closing failed')
			panic(err)
		}
	}
	mut app := new_app(db, setup_logger('./logs')!)
	app.mount_static_folder_at(os.resource_abs_path('.'), '/')
	vweb.run(app, http_port)
}

fn new_app(db &sqlite.DB, logger &log.Log) &App {
	mut app := &App{
		db: db
		logger: logger
	}

	return app
}

@['/'; get]
pub fn (mut app App) page_home() vweb.Result {
	return $vweb.html()
}

@['/ping/'; get]
pub fn (mut app App) ping() ?vweb.Result {
	return app.text('ping')
}

pub fn (mut app App) not_found() vweb.Result {
	app.set_status(404, 'Not Found')
	return app.html('<h1>Page not found</h1>')
}
