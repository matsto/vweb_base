module main

import vweb
import models
import services

@['/galleries/'; get]
pub fn (mut app App) page_galleries() vweb.Result {
	galleries := services.get_user_galleries(app.db, app.user.id) or { []models.Gallery{} }
	return $vweb.html()
}

@['/galleries/view/:id'; get]
pub fn (mut app App) page_carousel(id string) vweb.Result {
	photos := services.get_photos_by_gallery_id(app.db, id, app.user.id) or { []models.Photo{} }
	println(photos)
	if photos.len == 0 {
		return app.not_found()
	}return $vweb.html()
}

@['/galleries/:id'; get]
pub fn (mut app App) page_sidebar(id string) vweb.Result {
	photos := services.get_photos_by_gallery_id(app.db, id, app.user.id) or { []models.Photo{} }
	println(photos)
	if photos.len == 0 {
		return app.not_found()
	}
	return $vweb.html()
}
