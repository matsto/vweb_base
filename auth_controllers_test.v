module main

import vweb
import net.http
import json
import os
import rand
import time
import models

const port = 8181
const ip = '127.0.0.1'
const base_url = 'http://${ip}:${port}'
const logger_path = os.join_path_single(os.temp_dir(), '${rand.hex(5)}.db')
const db_path = ':memory:'

fn testsuite_begin() {
	db := setup_database(db_path) or { panic(err) }
	logger := setup_logger(logger_path) or { panic(err) }
	test_app := new_app(db, logger)
	spawn vweb.run(test_app, port)
	time.sleep(1 * time.second)
}

fn testsuite_end() {
	// add cleanup here
}

fn test_controller_auth() {
	create_url := base_url + '/users/'
	user := models.User{
		name: 'test_user'
		email: 'test_email'
		password: 'password'
	}
	req_body := json.encode(user)

	resp := http.post_json(create_url, req_body) or {
		assert err.msg() == '', err.msg()
		return
	}

	assert resp.status_code == 201

	url := base_url + '/auth/login/'

	auth_req := models.AuthRequest{user.name, user.password}
	auth_req_body := json.encode(auth_req)

	auth_resp := http.post_json(url, auth_req_body) or {
		assert err.msg() == ''
		return
	}
	assert auth_resp.status_code == 200, auth_resp.body
	assert auth_resp.body.len == 240, auth_resp.body
}
