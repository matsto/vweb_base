module main

import time
import os
import log

fn setup_logger(log_folder string) !log.Log {
	mut logger := log.Log{}
	logger.set_level(.debug)
	if !os.exists(log_folder) {
		os.mkdir(log_folder)!
	}

	logger.set_full_logpath('${log_folder}/log_${time.now().ymmdd()}.log')
	logger.log_to_console_too()
	return logger
}

pub fn (mut app App) warn(msg string) {
	app.logger.warn(msg)
	app.logger.flush()
}

pub fn (mut app App) info(msg string) {
	app.logger.info(msg)
	app.logger.flush()
}

pub fn (mut app App) debug(msg string) {
	app.logger.debug(msg)
	app.logger.flush()
}
