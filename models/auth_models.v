module models

import time

pub struct JwtHeader {
pub:
	alg string @[required]
	typ string @[required]
}

pub struct JwtPayload {
pub:
	sub         string    @[required] // (subject) = Entity to whom the token belongs, usually the user ID;
	iss         string // (issuer) = Token issuer;
	exp         time.Time @[required] // (expiration) = Timestamp of when the token will expire;
	iat         time.Time @[required] // (issued at) = Timestamp of when the token was created;
	aud         string // (audience) = Token recipient, represents the application that will use it.
	name        string    @[required]
	roles       string
	permissions string
}

pub struct AuthRequest {
pub:
	// Adding a [required] attribute will make decoding fail, if that field is not present in the input.
	// If a field is not [required], but is missing, it will be assumed to have its default value, like 0 for numbers, or '' for strings, and decoding will not fail.
	name     string @[required]
	password string @[required]
}

pub fn new_auth_request(form map[string]string) !AuthRequest {
	name := form['name']
	password := form['password']

	if name == '' {
		return error('name is required')
	}
	if password == '' {
		return error('password is required')
	}

	return AuthRequest{
		name: form['name']
		password: form['password']
	}
}
