tests:
	v test .

fmt:
	v fmt -w .

run:
	v watch run . -g

vet:
	v vet .
