module main

import vweb
import net.http
import models
import services

@['/login/'; get]
pub fn (mut app App) page_login() vweb.Result {
	token := app.get_cookie('token') or { '' }
	if token != '' {
		return app.redirect('/galleries/')
	}
	return $vweb.html()
}

@['/login/'; post]
pub fn (mut app App) controller_api_auth() vweb.Result {
	body := models.new_auth_request(app.form) or {
		app.set_status(400, '')
		return app.text('error: ${err}')
	}
	mut valid := 15
	if app.form['remember_me'] == 'on' {
		valid = 7 * 24 * 60
	}

	token := services.auth(app.db, body.name, body.password, valid) or {
		app.set_status(400, '')
		return app.text('error: ${err}')
	}
	app.set_cookie(http.Cookie{
		name: 'token'
		value: token
		path: '/'
		secure: true
		http_only: true
		max_age: valid * 60
	})
	return app.redirect('/galleries/')
}
