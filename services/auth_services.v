module services

import crypto.hmac
import crypto.sha256
import crypto.bcrypt
import encoding.base64
import json
import time
import os
import models
import db.sqlite

pub fn auth(db sqlite.DB, user_name string, password string, valid int) !string {
	user := get_by_name(db, user_name)!

	bcrypt.compare_hash_and_password(password.bytes(), user.password.bytes()) or {
		return error('Failed to auth user, ${err}')
	}

	token := make_token(user, valid)

	return token
}

pub fn make_token(user models.User, valid int) string {
	secret := os.getenv('SECRET_KEY')

	jwt_header := models.JwtHeader{'HS256', 'JWT'}
	issued := time.now()
	jwt_payload := models.JwtPayload{
		sub: user.id.str()
		name: user.name
		iat: issued
		exp: issued.add(valid * time.minute)
	}

	header := base64.url_encode(json.encode(jwt_header).bytes())
	payload := base64.url_encode(json.encode(jwt_payload).bytes())
	signature := base64.url_encode(hmac.new(secret.bytes(), '${header}.${payload}'.bytes(),
		sha256.sum, sha256.block_size).bytestr().bytes())

	jwt := '${header}.${payload}.${signature}'

	return jwt
}

pub fn auth_verify(token string) (bool, string) {
	if token == '' {
		return false, ''
	}
	secret := os.getenv('SECRET_KEY')
	token_split := token.split('.')

	signature_mirror := hmac.new(secret.bytes(), '${token_split[0]}.${token_split[1]}'.bytes(),
		sha256.sum, sha256.block_size).bytestr().bytes()

	signature_from_token := base64.url_decode(token_split[2])
	correct := hmac.equal(signature_from_token, signature_mirror)
	if correct {
		return correct, base64.url_decode_str(token_split[1])
	}

	return correct, ''
}
