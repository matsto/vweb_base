module main

import json
import dtos
import services
import regex
import net.http
import models

fn log_request_middleware(mut app App) {
	app.info('[Vweb] ${app.Context.req.method} ${app.Context.req.url}')
}

const safe_paths = {
	'GET':  [
		'/',
		'/ping/',
		'/login/',
	]
	'POST': [
		'/users/',
		'/login/',
	]
}

const static_file_paths = {
	'GET': [
		'/templates/assets/*',
	]
}

fn auth_middleware(mut app App) bool {
	if app.req.method == .options {
		return true
	}
	if app.req.url.to_lower() in safe_paths[app.req.method.str().to_upper()] {
		return true
	}
	templates := static_file_paths[app.req.method.str().to_upper()]
	for template in templates {
		query := '^' + template.replace('*', r'\S+') + '$'
		mut re := regex.regex_opt(query) or { continue }
		start, end := re.match_string(app.req.url.to_lower())
		if start == 0 && end == app.req.url.len_utf8() {
			return true
		}
	}

	content_type := app.get_header('Content-Type')
	authed := match content_type {
		'application/json' { auth_jwt_header(mut app) }
		else { auth_jwt_cookie(mut app) }
	}

	refresh_token_middleware(mut app)
	return authed
}

fn auth_jwt_cookie(mut app App) bool {
	cookie := app.get_cookie('token') or { '' }
	if cookie == '' {
		app.redirect('/login/')
		return false
	}
	return auth_jwt(mut app, cookie)
}

fn auth_jwt_header(mut app App) bool {
	auth_header := app.get_header('Authorization')
	if auth_header == '' || 'Bearer ' != auth_header[..7] {
		app.set_status(401, '')
		app.text('Missing Authorization header')
		return false
	}
	token := auth_header.all_after('Bearer ').trim(' ')
	return auth_jwt(mut app, token)
}

fn auth_jwt(mut app App, token string) bool {
	authed, payload := services.auth_verify(token)
	if !authed {
		app.set_status(401, '')
		app.text('HTTP 401: Forbidden')
		return false
	}
	user := json.decode(dtos.UserJWTData, payload) or {
		eprintln(err)
		app.set_status(500, '')
		app.text('HTTP 500: Internal Error')
		return false
	}
	app.user = user
	return true
}

fn refresh_token_middleware(mut app App) {
	valid := (app.user.exp - app.user.iat) / 60

	user := models.User{
		id: app.user.id.int()
		name: app.user.name
		password: ''
		email: ''
	}

	token := services.make_token(user, valid)
	app.set_cookie(http.Cookie{
		name: 'token'
		value: token
		path: '/'
		secure: true
		http_only: true
		max_age: valid * 60
	})
}
