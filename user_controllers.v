module main

import vweb
import json
import dtos
import models
import services

@['/users/'; post]
pub fn (mut app App) controller_create_user() vweb.Result {
	body := json.decode(models.User, app.req.data) or {
		app.set_status(400, '')
		return app.text('Failed to decode json, error: ${err}')
	}

	user := services.add_user(app.db, body.name, body.email, body.password) or {
		app.set_status(400, '')
		return app.text('error: ${err}')
	}
	user_schema := dtos.UserOutSchema{user.id, user.name, user.email, user.created_at, user.updated_at, user.active}

	app.set_status(201, '')
	return app.json(user_schema)
}

@['/users/'; get]
pub fn (mut app App) controller_get_all_user() vweb.Result {
	response := services.get_all_user(app.db) or {
		app.set_status(400, '')
		return app.text('${err}')
	}
	return app.json(response)
}

@['/users/:name'; get]
pub fn (mut app App) get_user(name string) vweb.Result {
	user := app.user

	if name != user.name {
		app.set_status(400, '')
		return app.text('User ${name} cannot be accessed')
	}

	response := services.get_by_name(app.db, name) or {
		app.set_status(400, '')
		return app.text('${err}')
	}
	return app.json(response)
}

@['/users/:name'; delete]
pub fn (mut app App) delete_user(name string) vweb.Result {
	user := app.user
	if name != user.name {
		app.set_status(400, '')
		return app.text('User ${name} cannot be deleted')
	}

	services.delete_user(app.db, name) or {
		app.set_status(400, '')
		return app.text('${err}')
	}
	app.set_status(203, '')
	return app.text('')
}
