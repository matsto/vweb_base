module services

import models
import db.sqlite

pub fn get_user_galleries(db sqlite.DB, user_id string) ![]models.Gallery {
	mut galleries := []models.Gallery{}

	rows := db.exec_param('SELECT
	gallery.id,
	gallery.name,
	gallery.active
	FROM gallery_user_linkage
	JOIN gallery
	WHERE user_id = ($1)',
		user_id)!
	for r in rows {
		galleries << models.Gallery{
			id: r.vals[0].int()
			name: r.vals[1].str()
			active: r.vals[2].bool()
		}
	}
	return galleries
}

pub fn get_photos_by_gallery_id(db sqlite.DB, gallery_id string, user_id string) ![]models.Photo {
	mut photos := []models.Photo{}

	rows := db.exec_param_many('SELECT
	p.id, p.path, `p.order`, p.gallery_id
	FROM photos p
	JOIN gallery_user_linkage g
	ON p.gallery_id = g.gallery_id
	WHERE g.user_id = ? AND g.gallery_id = ?',
		[user_id, gallery_id])!

	for r in rows {
		photos << models.Photo{
			id: r.vals[0].int()
			path: r.vals[1]
			order: r.vals[2].int()
			gallery_id: r.vals[3].int()
		}
	}
	return photos
}
